


/*
    Each function that you write must take at least 2s to execute using the setTimeout function like so:

    function() {
        setTimeout(() => {
            // Your code here
        }, 2 * 1000);
    }
*/

/* 
    Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/



let particularBoardInformationBasedOnTheBoardID = function (boardID,id,cb) {

    //check invalid conditions
    if((id && boardID && cb) == ( undefined || null) || typeof cb !== 'function' || !Array.isArray(boardID)){
        throw new Error("Invalid arguments");
    }
   return setTimeout(() => {

  
  
    //filter board id which is equal to given id and save in boardData.
    const bordData = boardID.filter(elemennt => elemennt.id === id)

    //passing board data to callback function.
    cb(bordData)
         
   }, 2000);
   
      
}

module.exports = particularBoardInformationBasedOnTheBoardID;
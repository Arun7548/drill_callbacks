const boardID = require('../Data/boards.json')
const listID = require('../Data/lists.json')
const cardsID = require('../Data/cards.json')

const myparticularBoardInformationBasedOnTheBoardID = require('../callback1')

let id = "mcu453ed"
try{
res = myparticularBoardInformationBasedOnTheBoardID(boardID,id,(cb)=>{
   const variable = cb.reduce((variable, element) => {
    if(element.id === id){
        variable = element.name
    }
    return variable
   }, "")

   console.log(variable)
})}  catch(e){
    console.log(e.message)
}


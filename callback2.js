
/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

let  allListsThatBelongToBoardBasedOnTheBoardIDThatIsPassedToIt = function(id,boardID,listID,cb){


    //check invalid conditions and go further.
    if((id && boardID && cb) == ( undefined || null) || typeof cb !== 'function' || !Array.isArray(boardID) || typeof listID !== 'object' ){
        throw new Error("Invalid arguments");
    }

  
    setTimeout(() => {
    //get board id which matches given id and save in variable.
    let bID = boardID.filter(item => item.id == id).map(element => element.id)

    //if listid matches with board id calling callback function. 
    let listdetail = Object.keys(listID).filter(item => {
        if(item == bID){
            cb(listID[bID])

    } })
        
    }, 2000);


}

module.exports = allListsThatBelongToBoardBasedOnTheBoardIDThatIsPassedToIt